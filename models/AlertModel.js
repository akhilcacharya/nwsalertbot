var mongoose = require('mongoose');

var alertSchema = mongoose.Schema({
    title: String,
    event: String,
    effective: String,
    expires: String,
    summary: String,
    severity: String,
    certainty: String,
    area: [],
    timestamp: Date, 
});




mongoose.model("Alert", alertSchema);
