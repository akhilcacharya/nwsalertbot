#NWSAlertBot

National Weather Service Alerts. In bot form. I'm just surprised this hasn't been done before. 

##Getting Started

* Clone/Download

* Get Twitter API key, MongoDB credentials

* Enter keys and other credentials in config.json

* Do ``npm install``

* Good to go! 

Currently, the delay is 7 minutes in milliseconds, but this can easily be changed by changing the delay in config.json. 


