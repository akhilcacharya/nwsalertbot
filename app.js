var config = require('./config.json'); 
var Twitter = require('twit'); 
var FeedParser = require('feedparser'); 
var http = require('http'); 

//DB
var mongoose = require('mongoose');
var db_url = config.db.base_url + config.db.username + ":" + config.db.password + config.db.path;

mongoose.connect(db_url);
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'Connection error'));

db.once('open', function callback() {
    console.log("Connected to remote MongoDB");
})

//Instantiate Model
require('./models/AlertModel.js'); 

var intervalId; 

var Bot = {
	start: function(){
		console.log("Welcome to NWSAlertBot"); 
		console.log("Now checking NWS Feed every " + config.delay/(60 * 1000) + " minutes"); 

		intervalId = setInterval(Bot.query, config.delay); 
		Bot.query(); 
	}, 

	query: function(){
		//Parse the latest feed

		var feedMeta; 
		var events = []; 

		http.get(config.api_url, function(response){
			response.pipe(new FeedParser({}))
				.on('error', function(error){
					console.log("There has been an error parsing the feed"); 
					console.log("Error: " + error); 
					console.log("Quitting now."); 
					clearInterval(intervalId); 
				})
				
				.on('meta', function(meta){
					feedMeta = meta; 
				})

				.on('readable', function(){
					var stream = this, item; 
					while(item = stream.read()){
						//cap namespace items return a hash, where the data is stored in an object with key "#"
						var event = {
							title: item['title'], 
							event: item['cap:event']['#'], 
							effective: item["cap:effective"]['#'], 
							expires: item['cap:expires']['#'], 
							summary: item['summary'].toLowerCase(), 
							severity: item['cap:severity']['#'], 
							certainty: item['cap:certainty']['#'], 
							area: item['cap:areadesc']['#'].split(';'),
							timestamp: new Date() 
						}; 

						events.push(event); 
					}
				})
				.on('end', function(){
					console.log("Weather events found"); 
					events.forEach(function(event){
						console.log(event); 
						console.log("\n"); 
					}); 
				}); 
		}); 


	}, 

	save: function(event){
		//Save the event and the ID


	}, 

	alert: function(){
		//Send Tweet

	}, 

	utils: {
		cleanXML: function(xml){
			return xml['#']; 
		}, 

	}
}

Bot.start(); 